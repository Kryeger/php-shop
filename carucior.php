<!DOCTYPE html>
<?php include('server.php'); ?>
<?php include('server_buy.php'); ?>

<style>
	* {
			margin: 0px; 
			padding: 0px; 
			box-sizing: border-box;
		}
		.mySlides {display: none}

		/* Next & previous buttons */
		.prev, .next {
		cursor: pointer;
		position: absolute;
		top: 50%;
		width: auto;
		padding: 16px;
		margin-top: -22px;
		color: white;
		font-weight: bold;
		font-size: 18px;
		transition: 0.6s ease;
		border-radius: 0 3px 3px 0;
		user-select: none;
		}

		/* Position the "next button" to the right */
		.next {
		right: 0;
		border-radius: 3px 0 0 3px;
		}

		/* On hover, add a black background color with a little bit see-through */
		.prev:hover, .next:hover {
		background-color: rgba(0,0,0,0.8);
		}

		/* Caption text */
		.text {
		color: #f2f2f2;
		font-size: 15px;
		padding: 8px 12px;
		position: absolute;
		bottom: 8px;
		width: 100%;
		text-align: center;
		}

		/* Number text (1/3 etc) */
		.numbertext {
		color: red;
		font-size: 12px;
		padding: 8px 12px;
		position: absolute;
		top: 0;
		}

		/* The dots/bullets/indicators */
		.dot {
		cursor: pointer;
		height: 15px;
		width: 15px;
		margin: 0 2px;
		background-color: #bbb;
		border-radius: 50%;
		display: inline-block;
		transition: background-color 0.6s ease;
		}

		.active, .dot:hover {
		background-color: #717171;
		}

		/* Fading animation */
		.fade {
		-webkit-animation-name: fade;
		-webkit-animation-duration: 1.5s;
		animation-name: fade;
		animation-duration: 1.5s;
		}

		@-webkit-keyframes fade {
		from {opacity: .4} 
		to {opacity: 1}
		}

		@keyframes fade {
		from {opacity: .4} 
		to {opacity: 1}
		}

		/* On smaller screens, decrease text size */
		@media only screen and (max-width: 300px) {
		.prev, .next,.text {font-size: 11px}
		}
		html{
			width:100%;
			height:100%;
			padding:0;
			margin:0;
		}
		body{
			width:100%;
			height:100%;
			padding:0;
			margin:0;
			overflow-x: hidden;
		}
		
		::-webkit-scrollbar {
			width: 10px;
		}
		::-webkit-scrollbar-track {
			background: #D3D3D3; 
		}
		::-webkit-scrollbar-thumb {
			background: #808080; 
		}
		::-webkit-scrollbar-thumb:hover {
			background: #707070; 
		}
		
		ul {
			position:fixed;
			top:0;
			list-style-type: none;
			height:8%;
			width:100%;
			margin: 0;
			padding: 5 5 5 0;
			overflow: hidden;
			background-color: #808080;
		}
		li.left{
			margin:0;
			padding:0;
			list-style-type: none;
		    overflow: hidden;
			float:left;
		}
		li.right{
			margin:0;
			padding:0;
			list-style-type: none;
		    overflow: hidden;
			float:right;
		}
		li a.home{	
			display: block;
			color: white;
			text-align: center;
			padding: 0;
			text-decoration: none;
		}
		li.iconhome{
			margin-left:2.5%;
		}
		li.menu{
			margin-left:2%;
			margin-top:1.5%;
		}
		li.cart{
			margin-right:3%;
			margin-top:0.75%;
		}
		li.login{
			margin-right:2%;
			margin-top:1.5%;
		}
		p.home{
			color:white;
		}
		p.login{
			color:white;
		}
		
		div.container1{
			width:95%;
			height:10%;
			margin-top:5%;
			margin-left:2.5%;
			padding:0;
			padding-bottom:10px;
			background-color:#808080;
		}
		div.divpret{
			float:right;
			width:40%;
			height:auto;
			margin:2.5%;
			margin-top:5%;
			padding:1%;
			background-color:#808080;
		}
		div.divimg{
			float:left;
			width:40%;
			height:auto;
			margin:2.5%;
			margin-top:5%;
			padding:1%;
			background-color:#808080;
		}
		p.textinfocont1{
			display:inline-block;
			margin:10px;
			padding:0;
			color:white;
		}
		p.linkcont1{
			display:inline-block;
			margin:0;
			padding:0;
			color:white;
		}
		a.linkcont1{
			text-decoration:none;
			margin:0;
			margin-left:86%;
			padding:0;
			width:auto;
			height:auto;
		}
		img.imgcont1{
			display:inline-block;
			width:46%;
			heght:100%;
			margin:10px;
			padding:0;
		}
		p.pret{
			margin:10px;
			padding:0;
			color:white;
		}
		p.img{
			margin:10px;
			padding:0;
			color:white;
		}
		form.form_size{
			margin:10px;
		}
		img.db_image{
			width:100%;
			height:750px;
		}
		div.container1{
			float:left;
			padding:10px;
			margin-top:5%;
			margin-left:25px;
			width:40%;
			height:auto;
			background-color:#808080;
		}
		div.container2{
			float:right;
			padding:10px;
			margin-top:5%;
			margin-right:25px;
			width:40%;
			height:auto;
			background-color:#808080;
		}
		div.slideshow-container {
		max-width: 1000px;
		position: relative;
		margin: auto;
		}
		a.link_stergere{
			text-decoration:none;
			color:black;
		}
		a.link_stergere:hover{
			color:red;
		}
		.button {
		background-color: #4CAF50; 
		border: none;
		color: white;
		padding: 16px 32px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin: 4px 2px;
		transition-duration: 0.4s;
		cursor: pointer;
		}
		.button3 {
		background-color: white; 
		color: red; 
		border: 2px solid #f44336;
		}

		.button3:hover {
		background-color: #f44336;
		color: black;
		}
		
	</style>
	
	<head> 
		<link rel="shortcut icon" href="./img/icon-title.ico" />
	</head>
	<title> Buy </title>
	<body bgcolor="#D3D3D3">
	<?php
	if(isset($_SESSION['username'])){
			// session_start();
			$username = '';
			$localhost = "localhost";
			$idbazadedate = "root";
			$parolabazadedate = "";
			$db = mysqli_connect($localhost , $idbazadedate , $parolabazadedate , 'shop');
			$id = $_SESSION['id'];
			$k = 1;
			$sql = "SELECT * FROM carucior WHERE id_user='$id'";
			$query = mysqli_query($db, $sql);
			echo '<div class="container1">';
				echo '<table border="1" width="100%" style="text-align:center;">';
				echo '<tr>';
						echo '<td>';
							echo 'Nr';
						echo '</td>';
						echo '<td>';
							echo 'Nume produs';
						echo '</td>';
						echo '<td>';
							echo 'Pret';
						echo '</td>';
						echo '<td>';
							echo 'Size';
						echo '</td>';
						echo '<td>';
							echo 'Sterge';
						echo '</td>';
					
				echo '</tr>';
				while($row = mysqli_fetch_assoc($query)){
					echo '<tr>';
					echo '<td>';
						echo $k;
					echo '</td>';
					if($row['tip_produs'] == 1){
						$idprodus = $row['id_produs'];
						$sql2 = "SELECT * FROM men WHERE id='$idprodus'";
						$query2 = mysqli_query($db, $sql2);
						$row2 = mysqli_fetch_assoc($query2);
						$pret = $row2['pret'] - '0.01';
						echo '<td>';
							echo $row2['name'];
						echo '</td>';
						echo '<td>';
							echo $pret . ' lei';
						echo '</td>';
						echo '<td>';
							echo $row['size'];
						echo '</td>';
						echo '<td>';
						echo '<a href="?stergere='.$row['id'].'" class="link_stergere">Sterge</a>';
						echo '</td>';
					}
					else{
						$idprodus = $row['id_produs'];
						$sql2 = "SELECT * FROM women WHERE id='$idprodus'";
						$query2 = mysqli_query($db, $sql2);
						$row2 = mysqli_fetch_assoc($query2);
						$pret = $row2['pret'] - '0.01';
						
						echo '<td>';
							echo $row2['name'];
						echo '</td>';
						echo '<td>';
							echo $pret . ' lei';
						echo '</td>';
						echo '<td>';
							echo $row['size'];
						echo '</td>';
						echo '<td>';
						echo '<a href="?stergere='.$row['id'].'" class="link_stergere">Sterge</a>';
						echo '</td>';
					}
					echo '</tr>';
					$k = $k + 1;
				}
				echo '</table>';
			echo '<form method="POST">';
			echo '<input type="submit" name="submit_order" class="button button3" value="Finalizare Comanda"></input>';
			echo '</form>';
			echo '</div>';

			if(isset($_GET['stergere'])){
				$id_produs = $_GET['stergere'];
				mysqli_query($db , "DELETE FROM carucior WHERE id = '$id_produs'");
				header("Refresh:0; url=carucior.php");
			}
			if(isset($_POST['submit_order'])){
				mysqli_query($db , "DELETE FROM carucior WHERE id_user = '$id'");
				?>
				<script type="text/javascript">
				window.location = "comments.php";
				</script>      
    			<?php
				
			}
		}else{
			echo "<script> location.href='/login.php'; </script>";
			die("Redirecting to login.php");
		  }
		?>
		<div class="container2">
			<div class="slideshow-container">
			<div class="mySlides fade">
				<div class="numbertext">1 / 3</div>
				<img src="./reclame/adv1.jpg" style="width:100%"></img>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">2 / 3</div>
				<img src="reclame/adv2.jpg" style="width:100%"></img>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">3 / 3</div>
				<img src="reclame/adv3.jpg" style="width:100%;"></img>
			</div>

			<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
			<a class="next" onclick="plusSlides(1)">&#10095;</a>
			
			<div style="text-align:center">
				<span class="dot" onclick="currentSlide(1)"></span> 
				<span class="dot" onclick="currentSlide(2)"></span> 
				<span class="dot" onclick="currentSlide(3)"></span> 
			</div>
			</div>
		</div>
		<div style="width:auto;height:auto;">
			<ul>
				<li class="left iconhome"><a class="home" href="action_login.php" title="Acasă"><img src="./img/icon-title.ico"></img></a></li>
				<li class="left menu"><a class="home" href="viewall_man_login.php" title="Bărbați"><p class="home">MEN</p></a></li>
				<li class="left menu"><a class="home" href="viewall_woman_login.php" title="Femei"><p class="home">WOMEN</p></a></li>
				<li class="right cart"><a class="home" href="carucior.php" title="Cărucior"><img src="./img/12.png"></img></a></li>
				<?php 
					echo '<li class="right login"><a class="home" href="index.php?logout='. '1' . '" title="Logout"><p class="login">Log out</p></a></li>';
					echo '<li class="right login"><p class="login">'. $_SESSION['username'] .'</p></li>';
					
				?>
			</ul>
		</div>
	</body>
	<script src="slider.js"></script>
</html>

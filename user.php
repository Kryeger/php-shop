<!DOCTYPE>
<?php include('server.php'); ?>
<html>
	<style>
		* {
			margin: 0px; 
			padding: 0px; 
		}
		html{
			width:100%;
			height:100%;
			padding:0;
			margin:0;
		}
		body{
			width:auto;
			height:auto;
			padding-top:10%;
			margin:0;
		}
		
		::-webkit-scrollbar {
			width: 10px;
		}
		::-webkit-scrollbar-track {
			background: #D3D3D3; 
		}
		::-webkit-scrollbar-thumb {
			background: #808080; 
		}
		::-webkit-scrollbar-thumb:hover {
			background: #707070; 
		}
		form.adauga{
			background-color:#ff4d4d;
			margin:auto;
			min-width:250;
			max-width:300;
			text-align:center;
			padding:2.5% 5% 1% 5%;
			font-size:20;
			border:1px solid #ff4d4d;
			border-radius:5px;
		}
		div.adauga{
			background-color:#ff4d4d;
			margin:auto;
			min-width:250;
			max-width:300;
			text-align:center;
			padding:0;
			font-size:20;
			border:0px solid #ff4d4d;
			border-radius:5px;
		}
		div.info{
			background-color:#ff4d4d;
			margin:auto;
			min-width:250;
			max-width:300;
			text-align:center;
			padding:0;
			padding-top:10;
			padding-bottom:10;
			font-size:20;
			border:0px solid #ff4d4d;
			border-radius:5px;
			margin-bottom:10;
		}
		a.menu{
			text-decoration:none;
			color:white;
			margin:0;
			padding:5;
		}
		a.menu:hover{
			cursor:pointer;
		}
		a.menu_checked{
			text-decoration:none;
			color:#ff4d4d;
			background-color:#d3d3d3;
			border-bottom:0px solid #d3d3d3;
			border-bottom-left-radius:5px;
			border-bottom-right-radius:5px;
			margin:0;
			padding:5;
			padding-top:0;
		}
		p.info{
			font-size:20;
			color:white;
			margin:0;
			padding:0;
		}
		label{
			color:white;
			padding:0;
		}
		input.text{
			width:auto;
			height:auto;
			min-width:70%;
			min-height:25;
			padding:0;
			margin:0;
			margin-top:10;
			margin-bottom:10;
			padding:2.5px;
			border:0px solid;
			border-radius:5px;
        }

		input.submit{
			color:black;
			margin:0;
			padding:0;
			margin-top:5px;
			margin-bottom:10px;
			padding:5 20 5 20;
			border:1px solid #ff4d4d;
			border-radius:5px;
		}
		input.submit:hover{
			cursor:pointer;
			background-color:#D3D3D3;
		}
	</style>
	<head> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="./img/icon-title.ico" />
	</head>
	
	<title> User edit </title>
	
	<body bgcolor="#D3D3D3">
		<div class="info">
			<p class="info">User: <?php echo $_SESSION['username']; ?></p>
		</div>
		<div class="adauga">
			<a class="menu_checked" href="admin_panel.php_edit">Editeaza</a>
			<a class="menu" href="admin_panel.php?logout_admin=1" title="Logout">Logout</a>
			<form class="adauga" method="POST">
				<label>Username</label><br>
				<input class="text" type="text" name="username" title="Username"></input><br>
				<label>Email</label><br>
				<input class="text" type="text"  name="email" title="Email"></input><br>
				<label>Password</label><br>
				<input class="text" type="text" name="password1" title="Password1"></input><br>
				<input class="submit" type="submit" name="userEdit" value="Edit Account"></input>
			</form>
		</div>
	</body>
</html>
<!DOCTYPE>
<?php include('server.php'); ?>
<html>
	<style>
		* {
			margin: 0px; 
			padding: 0px; 
		}
		html{
			width:100%;
			height:100%;
			padding:0;
			margin:0;
		}
		body{
			width:100%;
			height:100%;
			padding:0;
			margin:0;	
		}
		
		::-webkit-scrollbar {
			width: 10px;
		}
		::-webkit-scrollbar-track {
			background: #D3D3D3; 
		}
		::-webkit-scrollbar-thumb {
			background: #FF8DA1; 
		}
		::-webkit-scrollbar-thumb:hover {
			background: #707070; 
		}
		
		ul {
			position:fixed;
			top:0;
			list-style-type: none;
			height:7%;
			width:100%;
			margin: 0;
			padding: 5 5 5 0;
			overflow: hidden;
			background-color: #FF8DA1;
		}
		li.left{
			margin:0;
			padding:0;
			list-style-type: none;
		    overflow: hidden;
			float:left;
		}
		li.right{
			margin:0;
			padding:0;
			list-style-type: none;
		    overflow: hidden;
			float:right;
		}
		li a.home{	
			display: block;
			color: white;
			text-align: center;
			padding: 0;
			text-decoration: none;
		}
		li a.login{	
			display: block;
			color: white;
			text-align: center;
			padding: 0;
			text-decoration: none;
		}
		li.iconhome{
			margin-left:2.5%;
		}
		li.menu{
			margin-left:2%;
			margin-top:1.5%;
		}
		li.cart{
			margin-right:3%;
			margin-top:0.75%;
		}
		li.login{
			margin-right:2%;
			margin-top:1.5%;
		}
		p.home{
			color:gray;
		}
		p.login{
			color:gray;
		}
		
		div.container1{
			float:center;
			width:95%;
			height:auto;
			margin-top:5%;
			margin-left:2.5%;
			padding:0;
			padding-bottom:10px;
			background-color:#FF8DA1;
		}
		
		ul.container1_1{
			position:relative;
			list-style-type: none;
			height:auto;
			width:100%;
			margin:0;
			margin-top: 5%;
			padding: 5 0 10 0;
			overflow: hidden;
			background-color: #FF8DA1;
		}
		
		p.textinfocont1{
			margin:10px;
			padding:0;
			color:gray;
		}
		p.linkcont1{
			margin:10px;
			padding:0;
			color:grey;
		}
		a.linkcont1{
			text-decoration:none;
			margin:0;
			padding:0;
			width:auto;
			height:auto;
		}
		img.imgcont1{
			display:inline-block;
			width:18.8%;
			height:auto;
			margin:0;
			padding:0;
		}
		img.imgcont1:hover
		{
			transform: scale(1.2);
		}
		
		a.linkimgcont1{
			text-decoration:none;
			margin:0;
			margin-left:10px;
			padding:0;
			width:auto;
			height:auto;
		}
		
		
		ul.container2_1{
			position:relative;
			list-style-type: none;
			height:auto;
			width:100%;
			margin:0;
			padding: 5 0 10 0;
			overflow: hidden;
			background-color:#FF8DA1;
		}
		div.container2{
			float:center;
			width:95%;
			height:auto;
			margin:2.5%;
			padding:0;
			padding-bottom:10px;
			background-color:#FF8DA1;
		}
		p.textinfocont2{
			margin:10px;
			padding:0;
			color:grey;
		}
		p.linkcont2{
			margin:10px;
			padding:0;
			color:grey;
		}
		a.linkcont2{
			text-decoration:none;
			margin:0;
			padding:0;
			width:auto;
			height:auto;
	</style>
	<head> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="./img/icon-title.ico" />
	</head>
	
	<title>Your online fashion store</title>

	<body bgcolor="#FFC0CB">
		
		<div class="container1">
			<ul class="container1_1">
				<li class="left"><p class="textinfocont1" title="Bărbați">MEN</p></li>
				<li class="right"><a class="linkcont1" href="viewall_man_login.php" title="View all"><p class="linkcont1">View all</p></a></li>
			</ul>
				<a class="linkimgcont1" href="viewall_man_login.php" title="Tricouri"><img class="imgcont1" src="./barbati_img/tricoumain5.jpg"></img></a>
				<a class="linkimgcont1" href="viewall_man_login.php" title="Pantaloni scurți"><img class="imgcont1" src="./barbati_img/pantaloniscurtimain4.jpg"></img></a>
				<a class="linkimgcont1" href="viewall_man_login.php" title="Cămăși"><img class="imgcont1" src="./barbati_img/camasamain1.jpg"></img></a>
				<a class="linkimgcont1" href="viewall_man_login.php" title="Încălțăminte"><img class="imgcont1" src="./barbati_img/incaltamintemain2.jpg"></img></a>
				<a class="linkimgcont1" href="viewall_man_login.php" title="Jeans"><img class="imgcont1" src="./barbati_img/jeansmain3.jpg"></img></a>
		</div>
		
		<div class="container2">
			<ul class="container2_1">
				<li class="left"><p class="textinfocont2" title="Femei">WOMEN</p>
				<li class="right"><a class="linkcont2" href="viewall_woman_login.php" title="View all"><p class="linkcont2">View all</p></a>
			</ul>
			<a class="linkimgcont1" href="viewall_woman_login.php" title="Bluze"><img class="imgcont1" src="./femei_img/bluzamain1.jpg"></img></a>
			<a class="linkimgcont1" href="viewall_woman_login.php" title="Pantaloni scurți"><img class="imgcont1" src="./femei_img/pantaloniscurtimain1.jpg"></img></a>
			<a class="linkimgcont1" href="viewall_woman_login.php" title="Cămăși"><img class="imgcont1" src="./femei_img/camasamain1.jpg"></img></a>
			<a class="linkimgcont1" href="viewall_woman_login.php" title="Încălțăminte"><img class="imgcont1" src="./femei_img/incaltamintemain1.jpg"></img></a>
			<a class="linkimgcont1" href="viewall_woman_login.php" title="Jeans"><img class="imgcont1" src="./femei_img/jeansmain1.jpg"></img></a>
		</div>
		<div style="width:auto;height:auto;">
			<ul>
				<li class="left iconhome"><a class="home" href="action_login.php" title="Acasă"><img src="./img/icon-title.ico"></img></a></li>
				<li class="left menu"><a class="home" href="viewall_man_login.php" title="Bărbați"><p class="home">MEN</p></a></li>
				<li class="left menu"><a class="home" href="viewall_woman_login.php" title="Femei"><p class="home">WOMEN</p></a></li>
				<li class="right cart"><a class="home" href="carucior.php" title="Cărucior"><img src="./img/12.png"></img></a></li>
				<?php 
					echo '<li class="right login"><a class="home" href="index.php?logout='. '1' . '" title="Logout"><p class="login">Log out</p></a></li>';
					echo '<li class="right login"><a class="login" href="user.php" title="EditProfile"><p class="login">'. $_SESSION['username'] .'</p></a></li>';
				?>
			</ul>
		</div>
	</body>


</html>
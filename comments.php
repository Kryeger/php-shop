<!DOCTYPE>
<?php include('server.php'); ?>
<html lang="en">
  <style>
    .container {
	max-width: 640px;
	margin: 30px auto;
	background: #fff;
	border-radius: 8px;
	padding: 20px;
}

.margin-bottom {
	150px;
}

.comment {
	display: block;
	transition: all 1s;
}

.commentClicked {
	min-height: 0px;
	border: 1px solid #eee;
	border-radius: 5px;
	padding: 5px 10px
}

textarea {
	width: 100%;
	border: none;
	background: #E8E8E8;
	padding: 5px 10px;
	height: 50%;
	border-radius: 5px 5px 0px 0px;
	border-bottom: 2px solid #016BA8;
	transition: all 0.5s;
	margin-top: 15px;
}

button.primaryContained {
	background: #016ba8;
	color: #fff;
	padding: 10px 10px;
	border: none;
	margin-top: 0px;
	cursor: pointer;
	text-transform: uppercase;
	letter-spacing: 4px;
	box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.25);
	transition: 1s all;
	font-size: 10px;
	border-radius: 5px;
}

button.primaryContained:hover {
	background: #9201A8;
}
</style>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title>Comment Section!</title>
    <meta name="author" content="Codeconvey" />
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	
</head>
<body>
		
<div class="ScriptTop">
    <div class="rt-container">
        <div class="col-rt-4" id="float-right">
            <!-- AD -->
        </div>
        <div class="col-rt-2">
            <ul>
                <li><a href="action_login.php">Back to Home</a></li>
            </ul>
        </div>
    </div>
</div>

<header class="ScriptHeader">
    <div class="rt-container">
    	<div class="col-rt-12">
        	<div class="rt-heading">
            	<h1>Thank you for your purchase. Please leave a comment!</h1>
                <p>Your commment will be visible to everybody.</p>
            </div>
        </div>
    </div>
</header>

<section>
    <div class="rt-container">
          <div class="col-rt-12">
              <div class="Scriptcontent">
              
<!-- partial:index.partial.html -->
<section id="app">
    <div class="container">
      <div class="row">
        <div class="col-6">
          <div class="comment">
        <p>
          <?php
          if(isset($_SESSION['username'])){
          $localhost = "localhost";
          $idbazadedate = "root";
          $parolabazadedate = "";
          $db = mysqli_connect($localhost , $idbazadedate , $parolabazadedate , 'shop');
          $sql = "SELECT message, timestamp, user FROM comments";
          $result = mysqli_query($db, $sql);
          while($row = mysqli_fetch_assoc($result)){
            echo '<div>'.$row['user'].' - '.$row['timestamp'].' - '.$row['message'].'</div>';
          }
        }else{
          echo "<script> location.href='/login.php'; </script>";
          die("Redirecting to login.php");
          }
          ?> 
        </p>
          </div><!--End Comment-->
          </div><!--End col -->
          </div><!-- End row -->
      <div class="row">
        <div class="col-6">
          <form method="POST">
      <textarea type="text" name="message" class="input" placeholder="Write a comment"></textarea>
      <input type="text" name="user" class="input" placeholder="Write your name">
          <button class='primaryContained float-right' type="submit" name='comments'>Add Comment</button>
          </div>
        </div><!-- End col -->
      </div><!--End Row -->
    </div><!--End Container -->
  </section><!-- end App -->
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>

<!-- <script>
  $(document).ready(function(){ 
  
  $(".primaryContained").on('click', function(){
  $(".comment").addClass("commentClicked");
});//end click
$("textarea").on('keyup.enter', function(){
  $(".comment").addClass("commentClicked");
});//end keyup
});//End Function 
new Vue({
  el: "#app",
  data:{
     title: 'Add a comment',
    newItem: '',
    item: [],
  },
  created: function() {
    axios
      .get("/comments")
      .then(res => {
        this.item = $result;
      })
  }
  methods:{
    addItem  (){
    this.item.push(this.$sql = mysqli_query($db , "SELECT message FROM comments");
		$result = mysqli_query($db, $sql);newItem);
      this.newItem = "";
    }
}
});
</script> -->
           
 </div>
		</div>
    </div>
</section>
     

<!-- Analytics -->

	</body>
</html>